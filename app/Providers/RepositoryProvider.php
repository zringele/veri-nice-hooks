<?php
declare(strict_types = 1);

namespace App\Providers;

use App\Repository\Contracts\SubscriptionRepositoryContract;
use App\Repository\Contracts\TransactionRepositoryContract;
use App\Repository\SubscriptionRepository;
use App\Repository\TransactionRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    const REPOSITORY_BINDINGS = [
        SubscriptionRepositoryContract::class => SubscriptionRepository::class,
        TransactionRepositoryContract::class => TransactionRepository::class
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach (self::REPOSITORY_BINDINGS as $contract => $repository) {
            $this->app->bind($contract, $repository);
        }
    }
}
