<?php

namespace App\Providers;

use App\Events\InitialPurchase;
use App\Events\SubscriptionCanceled;
use App\Events\SuccessfulRecurringCharge;
use App\Events\UnsuccessfulRecurringCharge;
use App\Listeners\CancelSubscription;
use App\Listeners\CreateSubscription;
use App\Listeners\CreateTransaction;
use App\Listeners\UpdateSubscription;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        InitialPurchase::class => [
            CreateSubscription::class,
            CreateTransaction::class
        ],
        SuccessfulRecurringCharge::class => [
            UpdateSubscription::class,
            CreateTransaction::class,
//            ExtendAccess::class
        ],
        UnsuccessfulRecurringCharge::class => [
            UpdateSubscription::class,
//            CancelAccess::class
        ],
        SubscriptionCanceled::class => [
            CancelSubscription::class,
//            ScheduleAccessCancel::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
