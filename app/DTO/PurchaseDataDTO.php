<?php


namespace App\DTO;


class PurchaseDataDTO
{
    /**
     * @var array
     */
    private $data;

    public function __construct(array $data)
    {
//        We pretend to be DTO here
        $this->data = $data;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
