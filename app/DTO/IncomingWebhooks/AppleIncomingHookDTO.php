<?php


namespace App\DTO\IncomingWebhooks;


use App\Http\Requests\AppleIncommingWebhook;

class AppleIncomingHookDTO
{
    /**
     * @var array
     */
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getUserId()
    {
        return $this->data['user'];
    }

    public function getTransactionId()
    {
        return $this->data['latest_receipt_info']['transaction_id'];
    }

    public function getProductId()
    {
        return $this->data['latest_receipt_info']['product_id'];
    }

    public function getPurchaseTimestamp()
    {
        return $this->data['latest_receipt_info']['original_purchase_date_ms'];
    }

    public function getNotificationType()
    {
        return $this->data['notification_type'];
    }

    public function getAutoRenew()
    {
        return $this->data['auto_renew_status'];
    }
}
