<?php

namespace App\Events;

use App\DTO\PurchaseDataDTO;
use App\Events\Contracts\CreatesTransaction;
use App\Events\Contracts\ModifiesSubscription;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SuccessfulRecurringCharge implements ModifiesSubscription, CreatesTransaction
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var PurchaseDataDTO
     */
    private $dto;

    /**
     * Create a new event instance.
     *
     * @param PurchaseDataDTO $dto
     */
    public function __construct(PurchaseDataDTO $dto)
    {
        $this->dto = $dto;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function getSubscriptionData(): array
    {
        $data = $this->dto->getData();

        return [
            'user_id' => $data['user_id'],
            'status' => 'active',
            'auto_renew' => $data['auto_renew'],
            'purchase_timestamp' => $data['purchase_timestamp'],
            'foreign_product_id' => $data['product_id'],
        ];
    }

    public function isRecurringSuccess(): bool
    {
        return true;
    }

    public function getTransactionData(): array
    {
        $data = $this->dto->getData();

        return [
            'user_id' => $data['user_id'],
            'foreign_id' => $data['transaction_id'],
            'foreign_product_id' => $data['product_id'],
            'purchase_timestamp' => $data['purchase_timestamp'],
        ];
    }
}
