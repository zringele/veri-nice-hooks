<?php


namespace App\Events\Contracts;


interface ModifiesSubscription
{
    public function getSubscriptionData(): array;

    public function isRecurringSuccess(): bool;
}
