<?php


namespace App\Events\Contracts;


interface CreatesTransaction
{
    public function getTransactionData(): array;
}
