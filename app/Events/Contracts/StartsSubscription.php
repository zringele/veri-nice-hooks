<?php


namespace App\Events\Contracts;


interface StartsSubscription
{
    public function getSubscriptionData(): array;
}
