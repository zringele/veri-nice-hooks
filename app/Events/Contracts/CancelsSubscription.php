<?php


namespace App\Events\Contracts;


interface CancelsSubscription
{
    public function getUserId(): string;

    public function getProductId(): string;
}
