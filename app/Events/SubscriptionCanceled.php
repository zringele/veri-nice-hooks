<?php

namespace App\Events;

use App\DTO\PurchaseDataDTO;
use App\Events\Contracts\CancelsSubscription;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SubscriptionCanceled implements CancelsSubscription
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var PurchaseDataDTO
     */
    private $dto;

    /**
     * Create a new event instance.
     *
     * @param PurchaseDataDTO $dto
     */
    public function __construct(PurchaseDataDTO $dto)
    {
        $this->dto = $dto;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function getUserId(): string
    {
        return $this->dto->getData()['user_id'];
    }

    public function getProductId(): string
    {
        return $this->dto->getData()['product_id'];
    }
}
