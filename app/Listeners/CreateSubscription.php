<?php

namespace App\Listeners;

use App\Events\Contracts\StartsSubscription;
use App\Events\SubscriptionStarted;
use App\Repository\Contracts\SubscriptionRepositoryContract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateSubscription
{
    /**
     * @var SubscriptionRepositoryContract
     */
    private $subscriptionRepository;

    /**
     * Create the event listener.
     *
     * @param SubscriptionRepositoryContract $subscriptionRepository
     */
    public function __construct(SubscriptionRepositoryContract $subscriptionRepository)
    {
        $this->subscriptionRepository = $subscriptionRepository;
    }

    /**
     * Handle the event.
     *
     * @param  StartsSubscription  $event
     * @return void
     */
    public function handle(StartsSubscription $event)
    {
        $this->subscriptionRepository->create($event->getSubscriptionData());
    }
}
