<?php

namespace App\Listeners;

use App\Events\Contracts\ModifiesSubscription;
use App\Repository\Contracts\SubscriptionRepositoryContract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateSubscription
{
    /**
     * @var SubscriptionRepositoryContract
     */
    private $subscriptionRepository;

    /**
     * Create the event listener.
     *
     * @param SubscriptionRepositoryContract $subscriptionRepository
     */
    public function __construct(SubscriptionRepositoryContract $subscriptionRepository)
    {
        $this->subscriptionRepository = $subscriptionRepository;
    }

    /**
     * Handle the event.
     *
     * @param ModifiesSubscription $event
     * @return void
     */
    public function handle(ModifiesSubscription $event)
    {
        $this->subscriptionRepository->recurringUpdate($event->getSubscriptionData(), $event->isRecurringSuccess());
    }
}
