<?php

namespace App\Listeners;

use App\Events\Contracts\CreatesTransaction;
use App\Repository\Contracts\TransactionRepositoryContract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateTransaction
{
    /**
     * @var TransactionRepositoryContract
     */
    private $transactionRepository;

    /**
     * Create the event listener.
     *
     * @param TransactionRepositoryContract $transactionRepository
     */
    public function __construct(TransactionRepositoryContract $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Handle the event.
     *
     * @param CreatesTransaction $event
     * @return void
     */
    public function handle(CreatesTransaction $event)
    {
        $this->transactionRepository->create($event->getTransactionData());
    }
}
