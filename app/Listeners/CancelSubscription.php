<?php

namespace App\Listeners;

use App\Events\Contracts\CancelsSubscription;
use App\Events\Contracts\CreatesTransaction;
use App\Repository\Contracts\SubscriptionRepositoryContract;
use App\Repository\Contracts\TransactionRepositoryContract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CancelSubscription
{
    /**
     * @var SubscriptionRepositoryContract
     */
    private $subscriptionRepository;

    /**
     * Create the event listener.
     *
     * @param SubscriptionRepositoryContract $subscriptionRepository
     */
    public function __construct(SubscriptionRepositoryContract $subscriptionRepository)
    {
        $this->subscriptionRepository = $subscriptionRepository;
    }

    /**
     * Handle the event.
     *
     * @param CancelsSubscription $event
     * @return void
     */
    public function handle(CancelsSubscription $event)
    {
        $this->subscriptionRepository->cancel($event->getUserId(), $event->getProductId());
    }
}
