<?php
declare(strict_types=1);

namespace App\Services\IncomingWebhooksParser\Contracts;


interface IncomingWebhook
{
    /**
     * Tells us if webhook came from legitimate source
     *
     * @return bool
     */
    public function validate(): bool;

    /**
     * Does needed action with the webhook
     *
     * @return void
     */
    public function doAction(): void;
}
