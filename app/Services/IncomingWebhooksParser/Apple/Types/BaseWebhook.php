<?php
declare(strict_types=1);

namespace App\Services\IncomingWebhooksParser\Apple\Types;


use App\DTO\IncomingWebhooks\AppleIncomingHookDTO;
use App\DTO\PurchaseDataDTO;

class BaseWebhook
{
    /**
     * @var AppleIncomingHookDTO
     */
    protected $dto;

    public function __construct(AppleIncomingHookDTO $dto)
    {
        $this->dto = $dto;
    }

    public function getUniversalDTO(): PurchaseDataDTO
    {
        return new PurchaseDataDTO([
            'user_id' => $this->dto->getUserId(),
            'product_id' => $this->dto->getProductId(),
            'transaction_id' => $this->dto->getTransactionId(),
            'auto_renew' => $this->dto->getAutoRenew() === 'true',
            'purchase_timestamp' => $this->dto->getPurchaseTimestamp(),
        ]);
    }
}
