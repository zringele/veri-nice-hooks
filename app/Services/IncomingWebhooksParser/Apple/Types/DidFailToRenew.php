<?php
declare(strict_types=1);


namespace App\Services\IncomingWebhooksParser\Apple\Types;


use App\Services\IncomingWebhooksParser\Apple\Contracts\DoesWebhookAction;

class DidFailToRenew extends BaseWebhook implements DoesWebhookAction
{

    public function doAction(): void
    {
        \App\Events\UnsuccessfulRecurringCharge::dispatch($this->getUniversalDTO());
    }
}
