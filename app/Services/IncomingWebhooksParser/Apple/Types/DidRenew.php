<?php
declare(strict_types=1);


namespace App\Services\IncomingWebhooksParser\Apple\Types;


use App\Events\SuccessfulRecurringCharge;
use App\Services\IncomingWebhooksParser\Apple\Contracts\DoesWebhookAction;

class DidRenew extends BaseWebhook implements DoesWebhookAction
{

    public function doAction(): void
    {
        SuccessfulRecurringCharge::dispatch($this->getUniversalDTO());
    }
}
