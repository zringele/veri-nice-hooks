<?php
declare(strict_types=1);


namespace App\Services\IncomingWebhooksParser\Apple;


use App\DTO\IncomingWebhooks\AppleIncomingHookDTO;
use App\Services\IncomingWebhooksParser\Apple\Contracts\DoesWebhookAction;
use App\Services\IncomingWebhooksParser\Apple\Types\DidFailToRenew;
use App\Services\IncomingWebhooksParser\Apple\Types\DidRenew;
use App\Services\IncomingWebhooksParser\Apple\Types\InitialPurchase;
use App\Services\IncomingWebhooksParser\Apple\Types\SubscriptionCanceled;

class IncomingWebhook implements \App\Services\IncomingWebhooksParser\Contracts\IncomingWebhook
{
    const ACCEPTED_TYPES = [
        'INITIAL_BUY' => InitialPurchase::class,
        'DID_RENEW' => DidRenew::class,
        'DID_FAIL_TO_RENEW' => DidFailToRenew::class,
        'CANCEL' => SubscriptionCanceled::class
    ];

    /**
     * @var DoesWebhookAction
     */
    private $type;

    public function __construct(AppleIncomingHookDTO $dto)
    {
        $typeName = $dto->getNotificationType();
        $typeClass = self::ACCEPTED_TYPES[$typeName];
        $this->type = new $typeClass($dto);
    }

    public function validate(): bool
    {
        return true; //Now we just assume it is good hook
    }

    public function doAction(): void
    {
        if (!$this->validate()) return;

        $this->type->doAction();
    }
}
