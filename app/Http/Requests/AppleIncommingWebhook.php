<?php

namespace App\Http\Requests;

use App\Services\IncomingWebhooksParser\Apple\IncomingWebhook;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AppleIncommingWebhook extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'latest_receipt' => ['string'],
            'latest_receipt_info' => ['array'],
            'notification_type' => ['string', Rule::in(array_keys(IncomingWebhook::ACCEPTED_TYPES))],
            'auto_renew_status' => ['string'],
            'user' => ['string']
        ];
    }
}
