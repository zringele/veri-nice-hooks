<?php

namespace App\Http\Controllers;

use App\DTO\IncomingWebhooks\AppleIncomingHookDTO;
use App\Http\Requests\AppleIncommingWebhook;
use App\Services\IncomingWebhooksParser\Apple\IncomingWebhook as AppleWebhook;
use App\Services\IncomingWebhooksParser\Base\IncomingWebhookParser;
use Illuminate\Http\Request;

class IncomingWebhookController extends Controller
{
    public function appleServer(AppleIncommingWebhook $request)
    {
        $webhook = new AppleWebhook(new AppleIncomingHookDTO($request->validated()));
        $webhook->doAction();
        return 'OK';
    }
}
