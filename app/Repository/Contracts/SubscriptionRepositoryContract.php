<?php
declare(strict_types=1);

namespace App\Repository\Contracts;


use App\Models\Subscription;

interface SubscriptionRepositoryContract
{
    public function create(array $data): Subscription;

    public function recurringUpdate(array $data, bool $success): Subscription;

    public function cancel(string $userId, string $productId);
}
