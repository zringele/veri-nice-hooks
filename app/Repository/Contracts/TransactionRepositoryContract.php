<?php
declare(strict_types=1);

namespace App\Repository\Contracts;


use App\Models\Transaction;

interface TransactionRepositoryContract
{
    public function create(array $data): Transaction;
}
