<?php
declare(strict_types=1);

namespace App\Repository;


use App\Models\Transaction;
use App\Repository\Contracts\TransactionRepositoryContract;

class TransactionRepository implements TransactionRepositoryContract
{

    public function create(array $data): Transaction
    {
        return Transaction::create($data);
    }
}
