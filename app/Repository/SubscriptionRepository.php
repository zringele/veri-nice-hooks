<?php
declare(strict_types=1);

namespace App\Repository;


use App\Models\Access;
use App\Models\Subscription;
use App\Repository\Contracts\SubscriptionRepositoryContract;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Throwable;

class SubscriptionRepository implements SubscriptionRepositoryContract
{

    /**
     * @param array $data
     * @return Subscription
     * @throws Throwable
     */
    public function create(array $data): Subscription
    {
        DB::beginTransaction();
        try {

            $subscription = new Subscription();
            $subscription->valid_to = $this->getValidTo($data['foreign_product_id']);
            $subscription->status = 'active';
            $subscription->user_id = $data['user_id'];
            $subscription->foreign_product_id = $data['foreign_product_id'];
            $subscription->auto_renew = $data['auto_renew'];
            $subscription->save();

            Access::create([
                'user_id' => $data['user_id'],
                'resource_id' => $data['foreign_product_id'],
                'valid_to' => $subscription->auto_renew ? null : $subscription->valid_to
            ]);

        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();
        return $subscription;
    }

    public function recurringUpdate(array $data, $success = true): Subscription
    {
        $subscription = Subscription::where([
            'user_id' => $data['user_id'],
            'foreign_product_id'=> $data['foreign_product_id']
        ])->first();


        $subscription->valid_to = $success ? $this->getValidTo($data['foreign_product_id']) : null;
        $subscription->status =  $success ? 'active': 'paused';
        $subscription->auto_renew = $success;
        $subscription->save();


        return $subscription;
    }

    public function cancel(string $userId, string $productId)
    {
        $subscription = Subscription::where([
           'user_id' => $userId,
           'foreign_product_id' => $productId
        ])->first();

        if (!$subscription) {
            return;
        }

        $subscription->auto_renew = false;
        $subscription->status = 'canceled';
        $subscription->save();
    }

    protected function getValidTo($productId): Carbon
    {
        //We get valid to here somehow based on product i guess

        return Carbon::tomorrow();
    }

}
